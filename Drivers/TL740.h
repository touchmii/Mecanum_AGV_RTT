/*
 * @Description: 初始化陀螺仪通信线程
 * @Author: Dryad
 * @Date: 2019-08-01 15:09:33
 */

#pragma once

#include <rtthread.h>

#ifdef __cplusplus
extern "C"
{
#endif

    void TL740_Thread_Init(void);                                                      /* 初始化TL740线程 */
    rt_err_t TL740_Return_Data(rt_int32_t wait_time, float *z_rate, float *z_heading); /*等待TL740获取到正确数据，并返回*/
    void Clear_Accumulated_Error(float current_z_heading);                             /* 清除陀螺仪内部角度数据，并记录当前角度 */

#ifdef __cplusplus
}
#endif
