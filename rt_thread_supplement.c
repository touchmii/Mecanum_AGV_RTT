/*
 * @Description: 添加RT-Thread所需的SysTick_Handler中断服务函数和us级延时
 * @Author: Dryad
 * @Date: 2019-05-25 09:16:58
 */
#include <rtthread.h>
#include <stm32f4xx.h>

void rt_hw_us_delay(rt_uint32_t us)
{
    rt_uint32_t delta;
    us = us * (SysTick->LOAD / (1000000 / RT_TICK_PER_SECOND));
    delta = SysTick->VAL;
    if (delta < us)
    {
        /* wait current OSTick left time gone */
        while (SysTick->VAL < us)
            ;
        us -= delta;
        delta = SysTick->LOAD;
    }
    while (delta - SysTick->VAL < us)
        ;
}

void SysTick_Handler(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    rt_tick_increase();

    /* leave interrupt */
    rt_interrupt_leave();
}
