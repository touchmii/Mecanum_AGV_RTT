/*
 * @Description: 一些常用的数学运算
 * @Author: Dryad
 * @Date: 2019-06-17 19:21:55
 */
#pragma once
#include <arm_math.h>

class My_Math_Class
{
public:
    template <typename T>
    static T Range(const T &x, const T &a, const T &b); /* 将输入x调整到[a,b]或者[b,a]范围内 */
    template <typename T>
    static T Min(const T &a, const T &b) { return a < b ? a : b; } /* 求a,b的较小值 */
    template <typename T>
    static T Max(const T &a, const T &b) { return a > b ? a : b; } /* 求a,b的较大值 */
    template <typename T>
    static void Swap(T &i, T &j); /* 交换两者 */
    template <typename T>
    static T ABS(const T &i) { return ((i) > (T)0 ? (i) : -(i)); } /* 取绝对值 */

    static float Trans_Rad(float angle) { return angle / 180.0f * PI; } /* 转换为弧度 */
    static float Trans_Angle(float rad) { return rad * 180.0f / PI; }   /* 转换为角度 */
private:
};

/**
 * @description: 将输入x调整到[a,b]或者[b,a]范围内
 * @param {type} 
 * @return: 
 */
template <typename T>
T My_Math_Class::Range(const T &x, const T &a, const T &b)
{
    const T *min, *max;
    if (a >= b)
    {
        min = &b;
        max = &a;
    }
    else
    {
        max = &b;
        min = &a;
    }

    if (x < *min)
    {
        return *min;
    }
    else if (x > *max)
    {
        return *max;
    }
    return x;
}

/**
 * @description: 交换两者
 * @param {type} 
 * @return: 
 */
template <typename T>
void My_Math_Class::Swap(T &i, T &j)
{
    T temp = i;
    i = j;
    j = temp;
}
