/*
 * @Description: PID控制器,位置型PID
 * @Author: Dryad
 * @Date: 2019-07-08 08:23:49
 */
#pragma once

class PID_Control_Class
{
public:
    void Init(float Kp, float Ki, float Kd);                    /* 设置PID三个参数 */
    void Set_Limit(float increase_limit, float decrease_limit); /* 设置限幅 */
    float Cal(float e);                                         /* 根据误差计算控制量 */

private:
    float kp, ki, kd;
    float pre_e;
    float increase_limit, decrease_limit;
    float integral;
};
