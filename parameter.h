/*
 * @Description: 系统参数设置
 * @Author: Dryad
 * @Date: 2019-06-14 20:21:06
 */

#pragma once

/* AGV参数定义，映射到Modbus上，寄存器地址为序号+0x100 */
enum AGV_Parameter_Enum
{
    Parameter_AGV_Address = 1,    /* 从机地址 */
    Parameter_Obstacle_Velocity,  /* 有障碍物时的低速度(mm/s) */
    Parameter_Dec_Emergency_Time, /* 急停时的减速度时间(s) */

    Parameter_Default_Acc, /* 默认加速度时间(s)，从零速到最高速所需时间 */
    Parameter_Default_Dec, /* 默认减速度时间(s)，从最高速到零速所需时间 */

    Parameter_Motor_Max_RPM,         /* 电机最高转速(n/min) */
    Parameter_Moter_Min_RPM,         /* 电机最低转速(n/min) */
    Parameter_Wheel_Diameter,        /* 车轮直径(mm) */
    Parameter_Motor_Reduction_Ratio, /* 减速比 */

    Parameter_Distance_X, /* 两轮之间x轴距离 */
    Parameter_Disyance_Y, /* 两轮之间y轴距离 */

    Parameter_Overall_X, /* 车宽 */
    Parameter_Overall_Y, /* 车长 */

    Parameter_Max_Velocity,         /* 最大线速度(mm/s) */
    Parameter_Min_Velocity,         /* 最小线速度(mm/s) */
    Parameter_Max_Angular_Velocity, /* 最大角速度(°/s) */
    Parameter_Min_Angular_Velocity, /* 最大角速度(°/s) */

    Parameter_PGV_X,            /* PGV传感器x轴偏置(mm) */
    Parameter_PGV_Y,            /* PGV传感器y轴偏置(mm) */
    Parameter_PGV_Theta,        /* PGV传感器安装角度(°) */
    Parameter_PGV_Theta_Offset, /* PGV传感器theta偏置(°) */

    Parameter_WTD_Enable,     /* 看门狗使能标志 */
    Parameter_Avoid_Slave,    /* 避障传感器从机地址 */
    Parameter_Indicate_Slave, /* 运行指示从机地址 */
    Parameter_HMI_Slave,      /* HMI从机地址 */
};

struct AGV_Parameter_Struct
{
    char agv_address;         /* 从机地址 */
    float obstacle_velocity;  /* 有障碍物时的低速度(mm/s) */
    float dec_emergency_time; /* 急停时的减速度时间(s) */

    float default_acc; /* 默认加速度时间(s)，从零速到最高速所需时间 */
    float default_dec; /* 默认减速度时间(s)，从最高速到零速所需时间 */

    float motor_max_rpm;         /* 电机最高转速(n/min) */
    float motor_min_rpm;         /* 电机最低转速(n/min) */
    float wheel_diameter;        /* 车轮直径(mm) */
    float motor_reduction_ratio; /* 减速比 */

    float distance_x; /* 两轮之间x轴距离 */
    float distance_y; /* 两轮之间y轴距离 */

    float overall_x; /* 车宽 */
    float overall_y; /* 车长 */

    float max_velocity_line;        /* 最大线速度(mm/s) */
    float min_velocity_line;        /* 最小线速度(mm/s) */
    float max_angular_velocity;     /* 最大角速度(°/s) */
    float min_angular_velocity;     /* 最小角速度(°/s) */
    float agv_overall_offset_angle; /* 车外形的偏置角(°) */
    float distance_lx_ly;           /*运动学公式中的lx+ly(mm)*/

    float pgv_x;            /* PGV传感器x轴偏置(mm) */
    float pgv_y;            /* PGV传感器y轴偏置(mm) */
    float pgv_theta;        /* PGV传感器安装角度(°) */
    float pgv_theta_offset; /* PGV传感器theta偏置(°) */

    float acc_vx; /* 默认线加速度(mm/s^2) */
    float acc_vy; /* 默认线加速度(mm/s^2) */
    float acc_w;  /* 默认角加速度(°/s^2) */

    char wtd_enable_flag; /* 看门狗使能标志 */
    char avoid_slave;     /* 避障传感器从机地址 */
    char indicate_slave;  /* 运行指示从机地址 */
    char hmi_slave;       /* HMI从机地址 */
};

extern const struct AGV_Parameter_Struct *const agv_parameter; /* AGV参数 */

union parameter_data_union {
    char cvalue;
    unsigned short usvalue;
    unsigned int uivalue;
    float fvalue;
};
#ifdef __cplusplus
extern "C"
{
#endif

    void Init_Parameter(void);                                                                    /* 初始化参数 */
    void Updata_Parameter(enum AGV_Parameter_Enum index, const union parameter_data_union value); /* 更新指定参数 */
    void Set_Default_Parameter(void);                                                             /* 恢复出厂设置 */
    void Save_Parameter(void);                                                                    /* 保存参数 */

#ifdef __cplusplus
}
#endif
