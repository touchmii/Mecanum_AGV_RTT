#pragma once

#ifdef __CC_ARM
extern int Image$$RW_IRAM1$$ZI$$Limit;
#define STM32_SRAM_BEGIN ((void *)&Image$$RW_IRAM1$$ZI$$Limit)
#elif __ICCARM__
#pragma section = "HEAP"
#define STM32_SRAM_BEGIN (__segment_end("HEAP"))
#else
extern int __heap_start__;
#define STM32_SRAM_BEGIN (&__heap_start__)
#endif

#ifdef __ICCARM__
extern char __ICFEDIT_region_RAM_end__;
#define STM32_SRAM_END (&__ICFEDIT_region_RAM_end__)
#else
#define STM32_SRAM_SIZE 128
#define STM32_SRAM_END (0x20000000 + STM32_SRAM_SIZE * 1024)
#endif
