#include <rtthread.h>
#include <rthw.h>
#include "./led.h"
#include "../HALayer/stm32_gpio.hpp"

static void led_blink_thread(void *parameter); /* LED闪烁线程 */

static GPIO_Class led(GPIOC, GPIO_Pin_13); /* LED指示灯 */
static struct rt_thread led_thread;        /* LED闪烁线程句柄 */
ALIGN(RT_ALIGN_SIZE)                       /* 线程栈4字节对齐 */
static char led_thread_stack[160];         /* 线程栈起始地址 */

void led_blink_thread_init(void)
{
    rt_err_t rt_result;
    led.Init(GPIO_Mode_OUT);
    led.Set();
    rt_result = rt_thread_init(&led_thread, "led thread", led_blink_thread, RT_NULL, led_thread_stack, sizeof(led_thread_stack), 25, 1);
    if (rt_result == RT_EOK)
    {
        rt_thread_startup(&led_thread); /* 启动线程 */
    }
    else
    {
        rt_kprintf("led thread error\n");
    }
}

void led_blink_thread(void *parameter)
{
    while (1)
    {
        rt_thread_delay(800);
        led.Clear();
        rt_thread_delay(200);
        led.Set();
    }
}
