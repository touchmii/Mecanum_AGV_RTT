/*
 * @Description: LED闪烁线程
 * @Author: Dryad
 * @Date: 2019-05-25 08:56:53
 */
#pragma once
#ifdef __cplusplus
extern "C"
{
#endif

    void led_blink_thread_init(void);

#ifdef __cplusplus
}
#endif
