/*
 * @Description: 避障传感器检测
 * @Author: Dryad
 * @Date: 2019-10-08 06:55:51
 * @LastEditors: Dryad
 * @LastEditTime: 2019-10-08 06:57:35
 * @Encoding: utf-8
 */
#pragma once

#ifdef __cplusplus
extern "C"
{
#endif
    /*避障传感器避障状态定义*/
    enum Sensor_Enum
    {
        Sensor_Normal = 0, /* 避障未触发，正常运动 */
        Sensor_Alarm = 1,  /* 报警  */
        Sensor_Slow = 2,   /* 低速行驶 */
        Sensor_Stop = 3,   /* 快速停车 */
        Sensor_Brake = 4,  /* 车轮抱死 */
    };

    void Sensor_Check_Init(void);                  /* 初始化避障传感器 */
    enum Sensor_Enum Rear_Sensor_State(int index); /* 读取避障传感器 */
#ifdef __cplusplus
}
#endif
