/*
 * @Description: 避障
 * @Author: Dryad
 * @Date: 2019-10-15 10:29:51
 * @LastEditors: Dryad
 * @LastEditTime: 2019-10-15 14:19:19
 * @Encoding: utf-8
 */
#pragma once
#ifdef __cplusplus
extern "C"
{
#endif

    /*避障传感器避障状态定义*/
    enum Sensor_Enum
    {
        Sensor_Normal = 0, /* 避障未触发，正常运动 */
        Sensor_Alarm = 1,  /* 报警  */
        Sensor_Slow = 2,   /* 低速行驶 */
        Sensor_Stop = 3,   /* 快速停车 */
        Sensor_Brake = 4,  /* 车轮抱死 */
    };

    void Avoid_Init(char addr);             /* 初始化避障传感器读取线程 */
    void Avoid_Set_Area(char area);         /* 设置PBS传感器的检测区域 */
    enum Sensor_Enum Avoid_Read(int index); /* 读取避障传感器的值,0~3对应前后左右四个方向 */

#ifdef __cplusplus
}
#endif
