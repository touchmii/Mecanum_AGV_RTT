#pragma once
#ifdef __cplusplus
extern "C"
{
#endif
    enum Alarm
    {
        Alarm_IDLE = 0, /* 空闲 */
        Alarm_Norm,     /* 正常行走 */
        Alarm_Warning,  /* 警告 */
        Alarm_Error,    /* 错误 */
    };

    void Indicate_Init(char addr);     /* 指示灯线程初始化 */
    void Alarm_Set(enum Alarm alarm);  /* 设置警示灯 */
    void Music_Set(char index);        /* 设置音乐曲目 */
    void Move_Over_Output(char _flag); /* 运动结束输出 */

#ifdef __cplusplus
}
#endif
