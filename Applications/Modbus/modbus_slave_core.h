/*
 * @Description: Modbus Slave 从机实现
 * @Author: Dryad
 * @Date: 2019-09-06 09:00:57
 * @LastEditors: Dryad
 * @LastEditTime: 2019-10-07 21:52:14
 * @Encoding: utf-8
 */

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

    /* 初始化完成事件 */
    enum Init_Event_Enum
    {
        PGV_Initialization_Completed = 0, /* PGV初始化完成 */
        TL740_Initialization_Completed,   /* TL740初始化完成 */
        Motor_Initialization_Completed,   /* 电机初始化完成 */
        DM_Initialization_Completed,      /* 系统已经读取到了第一次二维码 */
    };

    /* 错误代码定义 */
    union Wrong_Code_Union {
        unsigned int wrong_code;
        struct
        {
            char timeout_no_set_wrong : 1; /* 未设置通信超时检测 */
            char timeout_wrong : 1;        /* 通信超时 */
            char sram_full : 1;            /* 内存不足 */
            int reserve : 29;              /* 保留位 */
        };
    };

    void Modbus_Slave_Core_Init(void);                                /* 初始化从机所需的互斥量 */
    int Modbus_Slave_Core(const char function_code, /* 功能码 */
                          const char *rx_data,      /* 数据域 */
                          const int rx_data_length, /* 数据域长度 */
                          char *tx_data             /* 应答数据 */
    );

    void Set_Init_Flag(enum Init_Event_Enum flag);               /* 设置初始化标志位 */
    void Set_Data_Matrix_Flag(char read_flag, unsigned int num); /* 设置二维码TAG号 */
    unsigned int Read_Wrong_Code(void);                          /* 读取错误代码 */

#ifdef __cplusplus
}
#endif
