/*
 * @Description: Modbus RTU 从机实现
 * @Author: Dryad
 * @Date: 2019-06-09 21:44:41
 */

#pragma once
#ifdef __cplusplus
extern "C"
{
#endif
    void Modbus_RTU_Slave_Thread_Init(unsigned long baudrate, char slave_id); /* 初始化Modbus Slave线程 */
#ifdef __cplusplus
}
#endif
