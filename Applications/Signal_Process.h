/*
 * @Description: 和信号采集副板通信
 * @Author: Dryad
 * @Date: 2019-10-08 06:52:00
 * @LastEditors: Dryad
 * @LastEditTime: 2019-10-08 06:54:16
 * @Encoding: utf-8
 */
#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

    enum Signal_Process_Alarm
    {
        Alarm_IDLE = 0, /* 空闲 */
        Alarm_Norm,     /* 正常行走 */
        Alarm_Warning,  /* 警告 */
        Alarm_Error,    /* 错误 */
    };

    void Signal_Process_Init(void);
    void Signal_Process_Set_Music(int index);
    void Signal_Process_Set_Alarm(enum Signal_Process_Alarm alarm);

#ifdef __cplusplus
}
#endif
