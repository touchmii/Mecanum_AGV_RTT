/*
 * @Description: AGV的定位线程
 * @Author: Dryad
 * @Date: 2019-07-03 10:06:15
 */
#pragma once
#ifdef __cplusplus
extern "C"
{
#endif

    void Location_Thread_Init(void); /* 初始化定位线程 */

    struct Location_Coor_Struct
    {
        float x_coor; /* x坐标(mm) */
        float y_coor; /* y坐标(mm) */
        float theta;  /* 角度(°) */
    };

    struct Location_Velocity_Struct
    {
        float vx; /* x方向速度(mm/s) */
        float vy; /* y方向速度(mm/s) */
        float w;  /* 角速度(°/s) */
    };

    void Read_Location_Coor(struct Location_Coor_Struct *coor);
    void Read_Location_Velocity(struct Location_Velocity_Struct *v);

#ifdef __cplusplus
}
#endif
