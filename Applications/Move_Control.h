/*
 * @Description: AGV的运动控制线程，负责AGV的直线运动，旋转运动
 * @Author: Dryad
 * @Date: 2019-07-03 10:00:00
 */
#pragma once
#include <rtthread.h>
#ifdef __cplusplus
extern "C"
{
#endif

    enum AGV_Move_State_Enum
    {
        Move_IDLE = 0,              /* 运动空闲 */
        Move_Stop_For_Waitting = 1, /* 停车等待,重定义为任务暂停 */
        Move_Trigger_Sensor = 2,    /* 正常运行时避障已触发 */
        Move_Normal = 3,            /* 正常运行 */
        Move_Over = 4,              /* 当前运动指令结束 */
        Move_Cancel = 5,            /* 运动任务取消中 */
    };                              /* AGV运动状态指示 */

    void Move_Control_Thread_Init(void);                /* 初始化运动控制线程 */
    enum AGV_Move_State_Enum Read_AGV_Move_State(void); /* 返回AGV运动状态 */
    rt_err_t Set_Destination(const void *destination);  /* 发送目标路径给Move_Control线程，此处应为Modbus_Slave线程接收到的曲线 */

    /* 事件集定义，两者使用的是同一个事件集 */
    enum Move_Control_Event_Enum
    {
        Move_Start = 1,        /* 任务启动 */
        Move_Control_Continue, /* 运动继续 */
        Move_Control_Pause,    /* 运动暂停 */
        Move_Control_Cancel,   /* 运动取消 */
    };                         /* 运动控制事件 */
    enum Motor_Brake_Event_Enum
    {
        First_Read_DM = 5,         /* 第一次读取到二维码 */
        Communication_Timeout = 6, /* Modbus通信超时 */
    };                             /* 电机抱闸事件 */

    void Write_Brake_Event(enum Motor_Brake_Event_Enum value);
    void Write_Move_Control_Event(enum Move_Control_Event_Enum value);

#ifdef __cplusplus
}
#endif
