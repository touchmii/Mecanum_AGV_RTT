/*
 * @Description: DS1232独立看门狗喂狗线程
 * @Author: Dryad
 * @Date: 2019-07-03 09:35:40
 */
#pragma once
#ifdef __cplusplus
extern "C"
{
#endif

    void watchdog_thread_init(void);

#ifdef __cplusplus
}
#endif
