#pragma once

/* RT-Thread 内核 */
#define RT_NAME_MAX 32                  /* 内核对象名称最大字节 */
#define RT_ALIGN_SIZE 4                 /* 4字节对齐 */
#define RT_THREAD_PRIORITY_MAX 32       /* 定义系统优先级数32 (0 - 31) */
#define RT_TICK_PER_SECOND 1000         /* 系统时钟1ms */
#define RT_USING_OVERFLOW_CHECK         /* 使能内存溢出检查 */
#define RT_USING_HOOK                   /* 使用钩子函数 */
#define RT_USING_IDLE_HOOK              /* 空闲钩子函数 */
#define RT_IDEL_HOOK_LIST_SIZE 4        /* 空闲线程钩子函数数量 */
#define IDLE_THREAD_STACK_SIZE 1024     /* 空闲线程堆栈大小 */
#define RT_USING_TIMER_SOFT             /* 使用软件定时器 */
#define RT_TIMER_THREAD_PRIO 4          /* 定时器线程优先级 */
#define RT_TIMER_THREAD_STACK_SIZE 1024 /* 定时器线程堆栈大小 */
#define RT_DEBUG                        /* DEBUG模式 */
#define RT_DEBUG_COLOR                  /* 有颜色的DEBUG信息 */

/* 线程间同步与通信 */
#define RT_USING_SEMAPHORE    /* 信号量 */
#define RT_USING_MUTEX        /* 互斥量 */
#define RT_USING_EVENT        /* 事件 */
#define RT_USING_MAILBOX      /* 邮箱 */
#define RT_USING_MESSAGEQUEUE /* 消息队列 */
#define RT_USING_SIGNALS      /* 信号 */

/* 自动初始化方式 */
#define RT_USING_COMPONENTS_INIT       /* 开启宏的自动初始化功能 */
#define RT_USING_USER_MAIN             /* 使用RTT的自动初始化功能 */
#define RT_MAIN_THREAD_STACK_SIZE 2048 /* 定义 main 线程的栈大小 */

/* 内存管理 */
#define RT_USING_MEMPOOL   /* 静态内存池 */
#define RT_USING_MEMHEAP   /* 开启两个或两个以上内存堆的拼接 */
#define RT_USING_SMALL_MEM /* 开启小内存管理算法 */
#define RT_USING_HEAP      /* 使用堆 */

/* 內核设备对象 */
#define RT_USING_DEVICE                /* 使用设备驱动 */
#define RT_USING_CONSOLE               /* 使用终端控制台 */
#define RT_CONSOLEBUF_SIZE 2048        /* 终端缓冲区大小 */
#define RT_CONSOLE_DEVICE_NAME "uart1" /* 终端设备名称 */

/* 设备驱动 */
#define RT_USING_SERIAL                    /* 使用串口驱动 */
#define RT_SERIAL_USING_DMA                /* 串口驱动使用DMA */
#define RT_SERIAL_RB_BUFSZ 128             /* 串口默认缓冲区大小 */
#define RT_USING_I2C                       /* 使用IIC驱动 */
#define RT_USING_I2C_BITOPS                /* 使用GPIO模拟IIC */
#define RT_USING_PIN                       /* 使用Pin设备 */
#define RT_USING_SPI                       /* 使用SPI设备 */
#define RT_USING_SPI_MSD                   /* 使用Micro SD卡 */
#define RT_USING_RTC                       /* 使用RTC设备 */
#define RT_USING_SFUD                      /* 使用SFUD驱动 */
#define RT_USING_MTD_NOR                   /* 使用MTD设备 */
#define RT_USING_SYSTEM_WORKQUEUE          /* 使用Workqueue */
#define RT_SYSTEM_WORKQUEUE_STACKSIZE 2048 /* 堆栈大小 */
#define RT_SYSTEM_WORKQUEUE_PRIORITY 20    /* 线程优先级 */

/* Finsh */
#define RT_USING_FINSH               /* 使用Finsh作为Shell */
#define FINSH_THREAD_NAME "rtshell"  /* Finsh线程名字 */
#define FINSH_USING_SYMTAB           /* 使用符号表，可以增加命令 */
#define FINSH_USING_DESCRIPTION      /* 对符号的描述 */
#define FINSH_USING_HISTORY          /* 打开历史功能 */
#define FINSH_HISTORY_LINES 5        /* 历史最大条数 */
#define FINSH_THREAD_PRIORITY 25     /* 线程优先级 */
#define FINSH_THREAD_STACK_SIZE 4096 /* 堆栈大小 */
#define FINSH_USING_MSH              /* 支持传统shell模式（msh） */
#define FINSH_USING_MSH_ONLY         /* 仅支持msh模式 */
#define FINSH_USING_MSH_DEFAULT      /* 默认为msh模式 */
#define FINSH_ARG_MAX 10             /* 最大输入参数数量 */

/* 设备 */
#define RT_USING_UART1
#define RT_USING_UART2
#define RT_USING_UART3
#define RT_USING_UART4
#define RT_USING_UART5
#define RT_USING_I2C1
#define RT_USING_FM24CL64
#define RT_USING_PCF8563
#define RT_USING_SPI1
#define RT_USING_SPI2

#define PGV_DEVICE_NAME "uart5"              /* PGV设备名称 */
#define TL740_DEVICE_NAME "uart4"            /* TL740设备名称 */
#define ModbusRTU_Master_DEVICE_NAME "uart3" /* ModbusRTU主机设备名称 */
#define ModbusRTU_Slave_DEVICE_NAME "uart2"  /* ModbusRTU从机设备名称 */

/* Ulog组件 */
#define RT_USING_ULOG                        /* 使用ulog组件 */
#define ULOG_OUTPUT_LVL_D                    /* 静态输出级别为Debug，比该级别低的语句将不会编译 */
#define ULOG_OUTPUT_LVL 7                    /* 静态输出级别 */
#define ULOG_USING_ISR_LOG                   /* 使能中断ISR日志功能 */
#define ULOG_ASSERT_ENABLE                   /* 使能断言检查 */
#define ULOG_LINE_BUF_SIZE 128               /* 日志最大长度128字节 */
#define ULOG_USING_ASYNC_OUTPUT              /* 使能异步输出模式 */
#define ULOG_ASYNC_OUTPUT_BUF_SIZE 8192      /* 异步输出缓冲区字节 */
#define ULOG_ASYNC_OUTPUT_BY_THREAD          /* 使用输出线程 */
#define ULOG_ASYNC_OUTPUT_THREAD_STACK 2048  /* 异步输出线程堆栈 */
#define ULOG_ASYNC_OUTPUT_THREAD_PRIORITY 30 /* 异步输出线程优先级 */

/* Ulog日志格式 */
#define ULOG_OUTPUT_FLOAT          /* 使能对浮点数的支持 */
#define ULOG_USING_COLOR           /* 带颜色的日志 */
#define ULOG_OUTPUT_TIME           /* 时间信息 */
#define ULOG_TIME_USING_TIMESTAMP  /* 时间戳 */
#define ULOG_OUTPUT_LEVEL          /* 级别信息 */
#define ULOG_OUTPUT_TAG            /* 标签信息 */
#define ULOG_BACKEND_USING_CONSOLE /* 使用终端输出 */

/* 虚拟文件系统DFS */
#define RT_USING_DFS               /* 使用设备虚拟文件系统 */
#define DFS_USING_WORKDIR          /* 使用工作目录 */
#define DFS_FILESYSTEMS_MAX 3      /* 挂载的文件系统的最大数量 */
#define DFS_FILESYSTEM_TYPES_MAX 3 /* 支持的文件系统的最大数量 */
#define DFS_FD_MAX 32              /* 打开文件的最大数据 */
#define RT_USING_DFS_ELMFAT        /* 开启elm-FasFS文件系统 */
#define RT_USING_DFS_DEVFS         /* 开启 DevFS 设备文件系统 */

/* elm-FatFs文件系统 */
#define RT_DFS_ELM_CODE_PAGE 437       /* 编码方式 */
#define RT_DFS_ELM_USE_LFN 3           /* 长文件名支持方式 */
#define RT_DFS_ELM_MAX_LFN 255         /* 文件名最大长度 */
#define RT_DFS_ELM_DRIVES 1            /* 挂载FatFS的设备数量 */
#define RT_DFS_ELM_MAX_SECTOR_SIZE 512 /* 文件系统扇区大小 */
#define RT_DFS_ELM_REENTRANT           /* 开启可重入性 */

/* POSIX层和C标准库 */
#define RT_USING_LIBC
#define RT_USING_POSIX

/* SFUD驱动 */
#define RT_SFUD_USING_SFDP /* 使用SFDP参数功能 */

/* FAL Flash抽象层 */
#define FAL_DEBUG 1                          /* 打开Debug模式 */
#define FAL_PART_HAS_TABLE_CFG               /* 分区表已定义 */
#define FAL_USING_SFUD_PORT                  /* 使用对SFUD的移植文件 */
#define FAL_USING_NOR_FLASH_DEV_NAME "25Q64" /* 板载SPI Flash设备名称 */

/* Little文件系统 */
#define LFS_READ_SIZE 256
#define LFS_PROG_SIZE 256
#define LFS_BLOCK_SIZE 4096
#define LFS_CACHE_SIZE 256
#define LFS_BLOCK_CYCLES 0
#define LFS_LOOKAHEAD_MAX 128
