#include "./stm32_gpio.hpp"

void GPIO_Class::Init(GPIO_InitTypeDef *GPIO_InitStructure)
{
	GPIO_InitStructure->GPIO_Pin = _pin;
	GPIO_Init(_gpio, GPIO_InitStructure);
}
void GPIO_Class::Init(GPIOMode_TypeDef mode, GPIOOType_TypeDef otype, GPIOPuPd_TypeDef pupd, GPIOSpeed_TypeDef speed)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Mode = mode;
	GPIO_InitStructure.GPIO_OType = otype;
	GPIO_InitStructure.GPIO_PuPd = pupd;
	GPIO_InitStructure.GPIO_Speed = speed;
	Init(&GPIO_InitStructure);
}
