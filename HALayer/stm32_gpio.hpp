#pragma once
#include <stm32f4xx_gpio.h>

//STM32F4 GPIO类
class GPIO_Class
{
public:
	GPIO_Class(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin) : _gpio(GPIOx), _pin(GPIO_Pin) {}
	GPIO_Class() = default;
	virtual ~GPIO_Class() = default;

	void Init(GPIO_InitTypeDef *GPIO_InitStructure);
	void Init(GPIOMode_TypeDef mode, GPIOOType_TypeDef otype = GPIO_OType_PP, GPIOPuPd_TypeDef pupd = GPIO_PuPd_NOPULL, GPIOSpeed_TypeDef speed = GPIO_Speed_25MHz);
	inline void Set(void) { _gpio->BSRRL = _pin; }
	inline void Clear(void) { _gpio->BSRRH = _pin; }
	inline bool Read(void) { return (bool)((_gpio->IDR) & _pin); }
	inline void Toggle(void) { (_gpio->ODR) ^= _pin; }
	void Write(bool value) { value ? Set() : Clear(); }

private:
	GPIO_TypeDef *_gpio; //GPIO基地址
	uint16_t _pin;
};
