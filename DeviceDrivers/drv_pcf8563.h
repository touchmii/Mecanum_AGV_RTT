/*
 * @Description: PCF8563时钟芯片驱动
 * @Author: Dryad
 * @Date: 2019-08-14 13:01:17
 * @LastEditors: Dryad
 * @LastEditTime: 2019-09-05 16:34:26
 * @Encoding: utf-8
 */

#pragma once
#ifdef __cplusplus
extern "C"
{
#endif

    struct pcf8563_time
    {
        unsigned char second;
        unsigned char minute;
        unsigned char hour;
        unsigned char day;
        unsigned char weekday;
        unsigned char month;
        unsigned char year; /* 从1900年起算 */
    };

    void PCF8563_Read_Time(struct pcf8563_time *value);
    void PCF8563_Set_Time(struct pcf8563_time *value);

#ifdef __cplusplus
}
#endif
