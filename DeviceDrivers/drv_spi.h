/*
 * @Description: SPI驱动程序
 * @Author: Dryad
 * @Date: 2019-08-12 20:37:34
 * @LastEditors: Dryad
 * @LastEditTime: 2019-09-05 16:33:54
 * @Encoding: utf-8
 */
#pragma once

#include <stm32f4xx_gpio.h>

#ifdef __cplusplus
extern "C"
{
#endif

    void stm32_spi_bus_attach(const char *bus_name,                        /* 总线名字 */
                              const char *device_name,                     /* 设备名称 */
                              GPIO_TypeDef *cs_gpiox, uint16_t cs_gpio_pin /* 片选脚 */
    );                                                                     /* 挂载SPI设备 */

#ifdef __cplusplus
}
#endif
