/*
 * @Description: 自己编写的基于rt-thread的can驱动，不符合rt-thread的设备驱动模型
 * @Author: Dryad
 * @Date: 2019-06-13 20:24:29
 */
#pragma once

#include <stm32f4xx_can.h>
#include <rtthread.h>

#ifdef __cplusplus
extern "C"
{
#endif

    /* CAN总线波特率定义 */
    typedef enum
    {
        CAN1MBaud,
        CAN800kBaud,
        CAN500kBaud,
        CAN250kBaud,
        CAN125kBaud,
        CAN100kBaud,
        CAN50kBaud,
        CAN20kBaud,
        CAN10kBaud,
    } CAN_Baud_enum;

    void Can_Init(const CAN_TypeDef *CANx, const CAN_Baud_enum baud);                               /* 配置Can接口，配置滤波器 */
    rt_err_t Can_Send(CAN_TypeDef *const CANx, const char *buf, const uint8_t length, uint32_t id); /* 通过CANx发送数据，返回发送邮箱 */
    rt_err_t Can_Set_Filter(CAN_TypeDef *CANx, const uint16_t id, rt_mq_t rx_message);              /* 设置滤波器和对应的接收数据邮箱 */

#ifdef __cplusplus
}
#endif
